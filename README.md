[![GitHub version](https://badge.fury.io/gh/Dr-cdfg%2FKC-Blog-Theme.svg)](https://badge.fury.io/gh/Dr-cdfg%2FKC-Blog-Theme)
# 主题
[KC Blog](https://www.kcblog.asia)网站官方主题，基于[Typecho](https://http://typecho.org/)默认主题修改，正在持续更新！

使用谷歌MD设计风格（MDUI）,部分UI及逻辑功能借鉴Materiality Typecho Theme。
<img src="https://github.com/Dr-cdfg/KC-Blog-Theme/raw/main/screenshot.png">
最后更新：2020.11.23（1.4.6）

1278467936@qq.com
# [MDUI](https://www.mdui.org)项目
 - 项目地址：https://github.com/zdhxiong/mdui
 - 项目文档：https://www.mdui.org/docs
 

# [Materiality Typecho Theme](https://www.eaimty.com/theme.html)项目
 - 项目地址：https://github.com/EAimTY/materiality-typecho-theme
 - 项目文档：https://www.eaimty.com/theme.html
 
# 注意事项
## 更多链接自定义
请遵循格式进行撰写：
```
<li class="mdui-menu-item"><a href="链接" class="mdui-ripple">链接文字</a></li>
```
多个项目：
```
<li class="mdui-menu-item"><a href="链接" class="mdui-ripple">链接文字</a></li> <li class="mdui-menu-item"><a href="链接" class="mdui-ripple">链接文字</a></li>
```
不需要添加分隔符
## 色调风格
颜色主题默可修改，请在安装后设置。

详细信息请至[MDUI设计文档-颜色与主题](https://www.mdui.org/docs/color)。

## 使用须知
1. [下载ZIP](https://github.com/Dr-cdfg/KC-Blog-Theme/archive/main.zip)
2. 在“网站根目录 - usr - themes”中创建新文件夹，并将代码zip解压到其中（根目录 - usr - themes - 你创建的文件夹 - 主题文件）
3. 在控制面板里开启主题并进行初次设置

# Bug反馈
作者邮箱：1278467936@qq.com
或者直接在Issues中反馈

# 开源协议

GNU GPL v3.0
 
