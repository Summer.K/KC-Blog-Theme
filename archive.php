<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>

    <div class="col-mb-12 col-8" id="main" role="main">
        <h3 class="archive-title"><?php $this->archiveTitle(array(
            'category'  =>  _t('分类 %s 下的文章'),
            'search'    =>  _t('包含关键字 %s 的文章'),
            'tag'       =>  _t('标签 %s 下的文章'),
            'author'    =>  _t('%s 发布的文章')
        ), '', ''); ?></h3>
        <?php if ($this->have()): ?>
    	<?php while($this->next()): ?>
        <article class="post" itaemscope itemtype="http://schema.org/BlogPosting" style="margin : 0px 7px 0px 7px;">

<div class="mdui-ripple mdui-hoverable mdui-card" href="<?php $this->permalink() ?>" style="padding: 15px;">
			<h2 class="post-title" itemprop="name headline"><a itemprop="url" href="<?php $this->permalink() ?>"><?php $this->title() ?></a></h2>

			<ul class="post-meta">
				<li itemprop="author" itemscope itemtype="http://schema.org/Person">
<div class="mdui-chip">
  <span class="mdui-chip-icon"><i class="mdui-icon material-icons">account_circle</i></span>
  <span class="mdui-chip-title"><a itemprop="name" href="<?php $this->author->permalink(); ?>" rel="author"><?php $this->author(); ?></a></span>
</div>
</li>
				<li>  <div class="mdui-chip"><span class="mdui-chip-icon"><i class="mdui-icon material-icons">date_range</i></span>
  <span class="mdui-chip-title"><time datetime="<?php $this->date('c'); ?>" itemprop="datePublished"><?php $this->date(); ?></time></span>
</div></li>
				<li><div class="mdui-chip"><span class="mdui-chip-icon"><i class="mdui-icon material-icons">folder</i></span>
  <span class="mdui-chip-title"><?php $this->category(','); ?></span>
</div></li>
				<li itemprop="interactionCount"><div class="mdui-chip"><span class="mdui-chip-icon"><i class="mdui-icon material-icons">people</i></span>
  <span class="mdui-chip-title"><a itemprop="discussionUrl" href="<?php $this->permalink() ?>#comments"><?php $this->commentsNum('发表评论', '1 条评论', '%d 条评论'); ?></a></span>
</div></li>
			</ul>
            <div class="post-content" itemprop="articleBody">

    			    			<?php $this->content(''); ?>
</div>
 


        </article>
	<?php endwhile; ?>
        <?php else: ?>
            <article class="post">
                <h2 class="post-title"><?php _e('没有找到内容'); ?></h2>
            </article>
        <?php endif; ?>

        <?php $this->pageNav('&laquo; 前一页', '后一页 &raquo;'); ?>
    </div><!-- end #main -->
<?php $this->need('footer.php'); ?>
